package at.danidipp.aau.vehicleAdministration;

import java.util.ArrayList;

public class ElectricCar extends Vehicle {
    private double batteryLevel;
    private double maxBatteryCapacity;
    private double powerConsumption;

    public ElectricCar(String name, Brand brand, ArrayList<Workshop> workshops, int weight, int maxPermissableWeight, double maxSpeed, double maxBatteryCapacity, double powerConsumption) {
        super(name, brand, workshops, weight, maxPermissableWeight, maxSpeed);
        this.batteryLevel = 0;
        this.maxBatteryCapacity = maxBatteryCapacity;
        this.powerConsumption = powerConsumption;
    }

    public void charge(double power, double hours){
        batteryLevel += power*hours;
        batteryLevel = batteryLevel > maxBatteryCapacity ? maxBatteryCapacity : batteryLevel;
    }

    @Override
    public double brake(){
        batteryLevel += getSpeed() > 0 ? 0.001 : 0;
        double newSpeed = Math.max(getSpeed()-10, 0);
        setSpeed(newSpeed);
        return newSpeed;
    }

    @Override
    public void drive(int kilometers){
        for(int i=0; i<kilometers; i++){
            accelerate();
            accelerate();
            accelerate();
            brake();
            brake();
            brake();
            if (batteryLevel >= powerConsumption/100) batteryLevel -= powerConsumption/100;
            else {
                System.out.println("Out of power! Drove " + i + " kilometers.");
                break;
            }
        }
    }

    @Override
    public void printInfo(){
        System.out.println("Vehicle #"+getId()+": \""+getName()+"\" by "+getBrand().getName()+".");
        System.out.println("Available workshops:");
        for(Workshop w : getWorkshops()) System.out.println(w.getName());
        System.out.println("Current weight: "+getWeight()+", max weight: "+ getMaxPermissableWeight());
        System.out.println("Current speed: "+getSpeed()+", max speed: "+getMaxSpeed());
        System.out.println("Current charge: "+batteryLevel+", max charge: "+maxBatteryCapacity+", power consumption: "+powerConsumption);
        System.out.println('\n');
    }

    public double getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(double batteryLevel) {
        this.batteryLevel = batteryLevel;
    }

    public double getMaxBatteryCapacity() {
        return maxBatteryCapacity;
    }

    public void setMaxBatteryCapacity(double maxBatteryCapacity) {
        this.maxBatteryCapacity = maxBatteryCapacity;
    }

    public double getPowerConsumption() {
        return powerConsumption;
    }

    public void setPowerConsumption(double powerConsumption) {
        this.powerConsumption = powerConsumption;
    }
}
