package at.danidipp.aau.vehicleAdministration;

import java.util.ArrayList;

public class Vehicle {
    private long id;
    private String name;
    private Brand brand;
    private ArrayList<Workshop> workshops;
    private int weight;
    private int maxPermissableWeight;
    private double speed;
    private double maxSpeed;

    private static long nextId = 0;

    public Vehicle(String name, Brand brand, ArrayList<Workshop> workshops, int weight, int maxPermissableWeight, double maxSpeed) {
        this.id = getNextId();
        this.name = name;
        this.brand = brand;
        this.workshops = workshops;
        this.weight = weight;
        this.maxPermissableWeight = maxPermissableWeight;
        this.speed = 0;
        this.maxSpeed = maxSpeed;
    }

    public double accelerate(){
        return (speed+10) > maxSpeed ? speed : speed+10;
    }

    public double brake(){
        return speed = Math.max(speed-10, 0);
    }

    public void drive(int kilometers){
        for(int i=0; i<kilometers; i++){
            accelerate();
            accelerate();
            accelerate();
            brake();
            brake();
            brake();
        }
    }

    public void printInfo(){
        System.out.println("Vehicle #"+id+": \""+name+"\" by "+brand.getName()+".");
        System.out.println("Available workshops:");
        for(Workshop w : workshops) System.out.println(w.getName());
        System.out.println("Current weight: "+weight+", max weight: "+ maxPermissableWeight);
        System.out.println("Current speed: "+speed+", max speed: "+maxSpeed);
        System.out.println('\n');
    }

    public Workshop getWorkshop(int postcode){
        for(Workshop w : workshops){
            if(w.getPostcode() == postcode) return w;
        }
        return null;
    }


    //getters and setters
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Brand getBrand() {
        return brand;
    }

    public void setBrand(Brand brand) {
        this.brand = brand;
    }

    public ArrayList<Workshop> getWorkshops() {
        return workshops;
    }

    public void setWorkshops(ArrayList<Workshop> workshops) {
        this.workshops = workshops;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getMaxPermissableWeight() {
        return maxPermissableWeight;
    }

    public void setMaxPermissableWeight(int maxPermissableWeight) {
        this.maxPermissableWeight = maxPermissableWeight;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    private static long getNextId(){return ++nextId;}
}
