package at.danidipp.aau.vehicleAdministration;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        Brand b1 = new Brand("Brand 1", "Country 1", "b11111111", "email@brand1.com");
        Brand b2 = new Brand("Brand 2", "Country 2", "b22222222", "email@brand2.com");

        Workshop w1 = new Workshop("Workshop 1", "Country 1", 1, "City 1", "Street 1", "w111111");
        Workshop w2 = new Workshop("Workshop 2", "Country 2", 2, "City 2", "Street 2", "w222222");
        Workshop w3 = new Workshop("Workshop 3", "Country 3", 3, "City 3", "Street 3", "w333333");

        ArrayList<Workshop> ws1 = new ArrayList<>();
        ws1.add(w1);
        ws1.add(w2);

        ArrayList<Workshop> ws2 = new ArrayList<>();
        ws2.add(w1);
        ws2.add(w3);


        Car v1 = new Car("Vehicle 1", b1, ws1, 1000, 3500, 130, 30, 1);
        ElectricCar v2 = new ElectricCar("Vehicle 2", b2, ws2, 1600, 4800, 160, 30, 1);

        v1.fillUp(31);
        v2.charge(15, 2);

        v1.drive(15);
        //v1.printInfo();
        v2.drive(3);
        v1.drive(6);
        //v2.printInfo();

        VehicleAdministration va = VehicleAdministration.getInstance();
        va.addVehicle(v1);
        va.addVehicle(v2);

        va.printAllVehicles();

        va.removeVehicle(v2.getId());
        va.search(v2.getId());
        va.search(v1.getId());
    }
}
