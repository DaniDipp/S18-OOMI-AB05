package at.danidipp.aau.vehicleAdministration;

import java.util.ArrayList;

public class VehicleAdministration {
    private static VehicleAdministration instance = null;
    private ArrayList<Vehicle> vehicles;

    private VehicleAdministration(){
        vehicles = new ArrayList<>();
    }

    public static VehicleAdministration getInstance(){
        return instance != null ? instance : new VehicleAdministration();
    }

    public void addVehicle(Vehicle vehicle){
        vehicles.add(vehicle);
    }

    public void addVehicle(Vehicle[] vehicles){
        for(Vehicle v : vehicles){
            this.vehicles.add(v);
        }
    }

    public void removeVehicle(int vehicleId){
        vehicles.removeIf(vehicle -> vehicle.getId() == vehicleId);
    }
    public void removeVehicle(long vehicleId){
        vehicles.removeIf(vehicle -> vehicle.getId() == vehicleId);
    }

    public void search(int vehicleId){
        for(Vehicle v : vehicles){
            if(v.getId() == vehicleId){
                v.printInfo();
                return;
            }
        }
        System.out.println("Vehicle #"+vehicleId+" was not found in the database.");
    }
    public void search(long vehicleId){
        for(Vehicle v : vehicles){
            if(v.getId() == vehicleId){
                v.printInfo();
                return;
            }
        }
        System.out.println("Vehicle #"+vehicleId+" was not found in the database.");
    }

    public void printAllVehicles(){
        for(Vehicle v : vehicles) v.printInfo();
    }

    public void search (String brandName){
        for(Vehicle v : vehicles){
            if(v.getBrand().getName() == brandName) v.printInfo();
        }
    }

    public void fillUpAllCars(double fuel){
        for(Vehicle v : vehicles){
            if(v instanceof Car) ((Car) v).fillUp(fuel);
        }
    }

    public void chargeUpAllElectricCars(double power, double hours){
        for(Vehicle v : vehicles){
            if(v instanceof ElectricCar) ((ElectricCar) v).charge(power, hours);
        }
    }
}
