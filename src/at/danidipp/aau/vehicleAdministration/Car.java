package at.danidipp.aau.vehicleAdministration;

import java.util.ArrayList;

public class Car extends Vehicle{
    private double fuel;
    private double maxFuel;
    private double fuelConsumption;

    public Car(String name, Brand brand, ArrayList<Workshop> workshops, int weight, int maxPermissableWeight, double maxSpeed, double maxFuel, double fuelConsumption){
        super(name, brand, workshops,weight, maxPermissableWeight, maxSpeed);
        this.fuel = 0;
        this.maxFuel = maxFuel;
        this.fuelConsumption = fuelConsumption;
    }

    public void  fillUp(double fuel){
        this.fuel += fuel;
        this.fuel = this.fuel > maxFuel ? maxFuel : this.fuel;
    }

    @Override
    public void drive(int kilometers) {
        for (int i = 0; i < kilometers; i++) {
            accelerate();
            accelerate();
            accelerate();
            brake();
            brake();
            brake();
            if (fuel >= fuelConsumption/100) fuel -= fuelConsumption/100;
            else {
                System.out.println("Out of fuel! Drove " + i + " kilometers.");
                break;
            }
        }
    }

    @Override
    public void printInfo(){
        System.out.println("Vehicle #"+getId()+": \""+getName()+"\" by "+getBrand().getName()+".");
        System.out.println("Available workshops:");
        for(Workshop w : getWorkshops()) System.out.println(w.getName());
        System.out.println("Current weight: "+getWeight()+", max weight: "+ getMaxPermissableWeight());
        System.out.println("Current speed: "+getSpeed()+", max speed: "+getMaxSpeed());
        System.out.println("Current fuel: "+fuel+", max fuel: "+maxFuel+", fuel consumption: "+fuelConsumption);
        System.out.println('\n');
    }

    public double getFuel() {
        return fuel;
    }

    public void setFuel(double fuel) {
        this.fuel = fuel;
    }

    public double getMaxFuel() {
        return maxFuel;
    }

    public void setMaxFuel(double maxFuel) {
        this.maxFuel = maxFuel;
    }

    public double getFuelConsumption() {
        return fuelConsumption;
    }

    public void setFuelConsumption(double fuelConsumption) {
        this.fuelConsumption = fuelConsumption;
    }
}
