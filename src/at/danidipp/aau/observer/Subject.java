package at.danidipp.aau.observer;

import java.util.ArrayList;
import java.util.List;

public  class Subject {
    private List<Observer> observers = new ArrayList<>();
    private long state;

    public void attach(Observer observer) {
        observers.add(observer);
    }

    public long getState() {
        return state;
    }

    public void setState(long value) {
        this.state = value;
        notifyAllObservers();
    }

    private void notifyAllObservers() {
        for (Observer observer : observers) {
            observer.update();
        }
    }

}
