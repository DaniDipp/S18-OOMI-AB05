package at.danidipp.aau.observer;

public class SomeSensorReader extends Observer {

	SomeSensor sensor;

	public SomeSensorReader(SomeSensor subject) {
		this.sensor = sensor;
		System.out.println("Start reading values...");
		
		//Thread t = new Thread(new Reader());
		//t.start();

        this.subject = subject;
        subject.attach(this);
	}

    @Override
    public void update() {
        System.out.println("SENSOR READER - Gelesener Sensorwert: " + subject.getState());
    }

    /**
	// Read sensor values in a two seconds interval
	private class Reader implements Runnable {

		@Override
		public void run() {
			while (true) {
				System.out.println("SENSOR READER - Gelesener Sensorwert: " + sensor.getCurrentValue());
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		}
	}
     **/
}
