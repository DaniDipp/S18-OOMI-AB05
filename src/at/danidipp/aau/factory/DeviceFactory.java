package at.danidipp.aau.factory;

import at.omi.smarthome.interfaces.Device;
import at.omi.smarthome.interfaces.Sensor;

public class DeviceFactory {

    public Device getDevice(String id, String name, String model){
        Device tempDevice;
        switch (model){
            case "A1": tempDevice = new SimulatedLight1Adapter(); break;
            case "A2": tempDevice = new SimulatedLight2Adapter(); break;
            case "S1":
                tempDevice = new SensorImpl();
                ((Sensor)tempDevice).setUnit("SomeUnit");
                break;
            default: return null;
        }
        tempDevice.setId(id);
        tempDevice.setName(name);
        return tempDevice;
    }
}
